/**
 * {@code winstonlogger} file provide the logger service.
 * This logger library is support winston version 2.4.4
 * @author Ketav Chotaliya
 * @since 1.0.1 RELEASE January 30, 2019
 */
'use strict'

const winston = require('winston')
const moment = require('moment')
const fs = require('fs')
const env = require('../config/env')

const ENVIRONMENTS = 'production'

// return the file name from absolute path for label in logs
const getLabel = function (fileName) {
  let parts = fileName.split('/')
  return parts[parts.length - 2] + '/' + parts.pop()
}

// return the file path for log file
const filePath = () => {
  let dir = __dirname + '/../logs'
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir)
  }
  return dir + `/logs_${moment.utc().format('YYYY-MM-DD')}_.log`
}

// define the logs level
let logLevel = constants.logLevels[0]
switch (env.ENVIRONMENT) {

  case 'demo':
  case 'qa':
    logLevel = 'silly'
    break
  case 'local':
  case 'development':
    logLevel = 'silly'
    break
}
if (env.LOG_LEVEL) {
  logLevel = env.LOG_LEVEL
}
console.log('logLevel >>>> ', logLevel)

// set file transport object
const fileOption = () => {
  return {
    level: logLevel,
    filename: filePath(),
    maxsize: 16777216, // Maximum size of a log file should be 16MB
    maxFiles: 64, // Maximum 64 file of 16 MB to be stored. i.e Max 1GB of logs can be stored
    handleExceptions: true,
    label: null, // Display file name
    json: false, // write error in json object or plain text
    timestamp: true,
    depth: true,
    colorize: false
    // silent: true    // Uncomment to turn off logging
  }
}

// set console transport object
const consoleOption = () => {
  return {
    level: logLevel,
    handleExceptions: true,
    label: null, // Display file name
    json: false, // write error in json object or plain text
    timestamp: true,
    depth: false,
    colorize: true // for colorrized error (i.e red for erroro, green for info)
    // silent: true // Uncomment to turn off logging
  }
}

// create transport array
const transportList = () => {
  return [
    new winston.transports.Console(consoleOption()),
    new winston.transports.File(fileOption())
  ]
}

// create winston logger object
const myLogger = () => {
  return new winston.Logger({
    transports: transportList(),
    exceptionHandlers: transportList()
  })
}

// export methods for external use
module.exports = () => {
  const logger = myLogger()
  return {
    error (fileName, method, msg, data) {
      this.setLabel(fileName, method)
      logger.error(msg, data ? data : '', '\n ============>>>>> \n')
    },
    warn (fileName, method, msg, data) {
      this.setLabel(fileName, method)
      logger.warn(msg, data ? data : '', '\n ============>>>>> \n')
    },
    info (fileName, method, msg, data) {
      this.setLabel(fileName, method)
      logger.info(msg, data ? data : '', '\n ============>>>>> \n')
    },
    verbose (fileName, method, msg, data) {
      this.setLabel(fileName, method)
      logger.verbose(msg, data ? data : '', '\n ============>>>>> \n')
    },
    debug (fileName, method, msg, data) {
      this.setLabel(fileName, method)
      logger.debug(msg, data ? data : '', '\n ============>>>>> \n')
    },
    silly (fileName, method, msg, data) {
      this.setLabel(fileName, method)
      logger.silly(msg, data ? data : '', '\n ============>>>>> \n')
    },
    setFileLevel (level) {
      logger.transports.file.level = level
    },
    setConsoleLevel (level) {
      logger.transports.console.level = level
    },
    setLabel (fileName, method = null) {
      let label = getLabel(fileName)
      label += method ? ' ~ ' + method : ''
      logger.transports.console.label = label
      logger.transports.file.label = label
    }
  }
}
